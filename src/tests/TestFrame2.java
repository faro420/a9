package tests;
/**
 * TeamNr:S1T5 <Mir Farshid, Baha> (<2141801>,<mirfarshid.baha@haw-hamburg.de>),
 * <Mehmet, Cakir> (<2195657>,<mehmet.cakir@haw-hamburg.de>),
 */
import component.ComposedResistor;
import component.ParallelResistor;
import component.Potentiometer;
import component.ResistanceNet;
import component.Resistor;
import component.SeriesResistor;

public class TestFrame2 {
    public static void main(String[] args) {
        ResistanceNet R1 = new Resistor("R1", 100.0);
        ResistanceNet R2 = new Resistor("R2", 200.0);
        ResistanceNet R3 = new Resistor("R3", 300.0);
        Potentiometer R4 = new Potentiometer("R4");
        ResistanceNet R5 = new Resistor("R5", 500.0);
        ResistanceNet R6 = new Resistor("R6", 600.0);
        // (R1|R3)+R2
        ComposedResistor r1r3 = new ParallelResistor(R1, R3);
        ComposedResistor r1r3r2 = new SeriesResistor(r1r3, R2); // R4+R5
        ComposedResistor r4r5 = new SeriesResistor(R4, R5);
        // ((R1|R3)+R2)|(R4+R5)|R6
        ComposedResistor total = new ParallelResistor(r1r3r2, r4r5, R6);
        int x = 0;
        System.out.println("R4    total Resistance");
        while (x <= 4000) {
            R4.setOhm(x);
            System.out.printf("%d    %.2f%n", x, total.getOhm());
            x += 400;
        }
    }
}
