package tests;
/**
 * TeamNr:S1T5 <Mir Farshid, Baha> (<2141801>,<mirfarshid.baha@haw-hamburg.de>),
 * <Mehmet, Cakir> (<2195657>,<mehmet.cakir@haw-hamburg.de>),
 */
import component.ComposedResistor;
import component.ParallelResistor;
import component.ResistanceNet;
import component.Resistor;
import component.SeriesResistor;

public class Testen {
    public static void main(String[] args) {
//        ResistanceNet r1 = new Resistor("R1", 50.0);
//        ResistanceNet r2 = new Resistor("R2", 35.0);
//        ResistanceNet r3 = new Resistor("R3", 15.0);
//        ResistanceNet r4 = new Resistor("R4", 40.0);
//        ComposedResistor combo = new SeriesResistor(r1, r2, r3, r4);
//        System.out.println(combo.getOhm());
//        ResistanceNet r1 = new Resistor("R1", 50.0);
//        ResistanceNet r2 = new Resistor("R2", 35.0);
//        ResistanceNet r3 = new Resistor("R3", 15.0);
//        ResistanceNet r4 = new Resistor("R4", 40.0);
//        ComposedResistor combo = new ParallelResistor(r1,r2,r3,r4);
//        System.out.println(combo.toString());
        
        ComposedResistor r = new ParallelResistor(new Resistor("R1",4600),new Resistor("R2",5750));
        System.out.println(r.getOhm());
    }

}
