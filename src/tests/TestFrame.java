package tests;
/**
 * TeamNr:S1T5 <Mir Farshid, Baha> (<2141801>,<mirfarshid.baha@haw-hamburg.de>),
 * <Mehmet, Cakir> (<2195657>,<mehmet.cakir@haw-hamburg.de>),
 */
import component.ComposedResistor;
import component.ParallelResistor;
import component.Potentiometer;
import component.Resistor;
import component.ResistanceNet;
import component.SeriesResistor;

public class TestFrame {
    public static void main(String[] args) {
        ResistanceNet R1 = new Resistor("R1", 100.0);
        ResistanceNet R2 = new Resistor("R2", 200.0);
        ResistanceNet R3 = new Resistor("R3", 300.0);
        ResistanceNet R4 = new Resistor("R4", 400.0);
        ResistanceNet R5 = new Resistor("R5", 500.0);
        ResistanceNet R6 = new Resistor("R6", 600.0);
        // (R1|R3)+R2
        ComposedResistor r1r3 = new ParallelResistor(R1, R3);
        ComposedResistor r1r3r2 = new SeriesResistor(r1r3, R2); // R4+R5
        ComposedResistor r4r5 = new SeriesResistor(R4, R5); //
        // ((R1|R3)+R2)|(R4+R5)|R6
        ComposedResistor total = new ParallelResistor(r1r3r2, r4r5, R6);
        System.out.printf("%.2f\n", total.getOhm());
        System.out.println(total.getCircuit());
        System.out.println(total.getNumberOfResistors());
    }
}
