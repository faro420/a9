package component;

/**
 * TeamNr:S1T5 <Mir Farshid, Baha> (<2141801>,<mirfarshid.baha@haw-hamburg.de>),
 * <Mehmet, Cakir> (<2195657>,<mehmet.cakir@haw-hamburg.de>),
 */
public class Resistor implements ResistanceNet, EncapsulatedDouble {
    private final String name;
    private final EncapsulatedDouble value;

    
    public Resistor(String name, EncapsulatedDouble ohm) {
        this.name = name;
        value = ohm;
    }

    public Resistor(String name, double ohm) {
        this(name, new ConstantDouble(ohm));
    }

    
    
    public String getCircuit() {
        return name;
    }

    public EncapsulatedDouble getValue() {
        return value;
    }
    
    public double getOhm() {
        return value.getOhm();
    }

    public int getNumberOfResistors() {
        return 1;
    }

    public String toString() {
        return String.format("[<%s>: %s, %s]", Resistor.class.getSimpleName(), name, value.toString());
    }
}
