package component;

public class ConstantDouble implements EncapsulatedDouble {
    private final double ohm;

    public ConstantDouble(double ohm) {
        this.ohm = ohm;
    }

    public double getOhm() {
        return ohm;
    }
    
    public String toString() {
        return String.format("[<%s>:%.2f]", ConstantDouble.class.getSimpleName(),ohm);
    }
}
