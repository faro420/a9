package component;

/**
 * TeamNr:S1T5 <Mir Farshid, Baha> (<2141801>,<mirfarshid.baha@haw-hamburg.de>),
 * <Mehmet, Cakir> (<2195657>,<mehmet.cakir@haw-hamburg.de>),
 */
import java.util.Arrays;

public class ParallelResistor extends ComposedResistor {
    public ParallelResistor(ResistanceNet... parallel) {
        super(parallel);
    }

    public double getOhm() {
        final ResistanceNet[] subnets = getSubnets();
        double[] leitwert = new double[subnets.length];
        double sumLeitwerte = 0.0;
        for (int i = 0; i < subnets.length; i++) {
            leitwert[i] = 1/subnets[i].getOhm();
        }
        Arrays.sort(leitwert);
        for (int i = 0; i < leitwert.length; i++) {
            sumLeitwerte = sumLeitwerte + leitwert[i];
        }
        return 1/sumLeitwerte;
    }

    @Override
    public String getCircuit() {
        return super.getCircuit("|");
    }

    public String toString() {
        return String.format("[<%s>: %s]",ParallelResistor.class.getSimpleName(), super.toString());
    }
}
