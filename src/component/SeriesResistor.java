package component;

/**
 * TeamNr:S1T5 <Mir Farshid, Baha> (<2141801>,<mirfarshid.baha@haw-hamburg.de>),
 * <Mehmet, Cakir> (<2195657>,<mehmet.cakir@haw-hamburg.de>),
 */
import java.util.Arrays;

public class SeriesResistor extends ComposedResistor {
    public SeriesResistor(ResistanceNet... series) {
        super(series);
    }

    public double getOhm() {
        final ResistanceNet[] subnets = getSubnets();
        double[] results = new double[subnets.length];
        for (int i = 0; i < subnets.length; i++) {
            results[i] = subnets[i].getOhm();
        }
        Arrays.sort(results);
        for (int i = 1; i < results.length; i++) {
            results[0] = results[0] + results[i];
        }
        return results[0];
    }

    @Override
    public String getCircuit() {
        return super.getCircuit("+");
    }


    public String toString() {
        return String.format("[<%s>: %s]", SeriesResistor.class.getSimpleName(), super.toString());
    }
}
