package component;

/**
 * TeamNr:S1T5 <Mir Farshid, Baha> (<2141801>,<mirfarshid.baha@haw-hamburg.de>),
 * <Mehmet, Cakir> (<2195657>,<mehmet.cakir@haw-hamburg.de>),
 */
public class Potentiometer extends Resistor {
  
    public Potentiometer(String name, EncapsulatedDouble ohm) {
      super(name, ohm);
    }
    
    public Potentiometer(String name, double ohm){
        this(name, new VariableDouble(ohm));
    }
    
    public Potentiometer(String name){
        this(name, 1000.0);
    }

    public void setOhm(double x) {
        final EncapsulatedDouble value = getValue();
        assert value instanceof VariableDouble : String.format( "Internal programming error : Type is false resp. %s",  value.getClass().getSimpleName() );
        ((VariableDouble) value).setOhm(x);
    }


    public String toString() {
        return String.format("[<%s>: %s]", Resistor.class.getSimpleName(), super.toString());
    }
}
