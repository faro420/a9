package component;

import java.util.Arrays;

public abstract class ComposedResistor implements ResistanceNet {
    private final ResistanceNet[] subnets;
    
    public ComposedResistor(ResistanceNet... subnets) {
        this.subnets = subnets;
    }

    public ResistanceNet[] getSubnets(){
        return subnets;
    }
    
    public String getCircuit(final String sign) {
        String s = "(" + subnets[0].getCircuit();
        for (int i = 1; i < subnets.length; i++){
            s = s + sign + subnets[i].getCircuit();
        }
        return s + ")";
    }

    public int getNumberOfResistors() {
        int n = 0;
        for (int i = 0; i < subnets.length; i++) {
            n = n + subnets[i].getNumberOfResistors();
        }
        return n;
    }

    
    public String toString() {
        return String.format("[<%s>: %s]",ComposedResistor.class.getSimpleName(),Arrays.toString(subnets));
    }
    
}
