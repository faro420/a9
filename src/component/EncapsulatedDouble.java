package component;

public interface EncapsulatedDouble {
    double getOhm();
}
