package component;
/**
 * TeamNr:S1T5 <Mir Farshid, Baha> (<2141801>,<mirfarshid.baha@haw-hamburg.de>),
 * <Mehmet, Cakir> (<2195657>,<mehmet.cakir@haw-hamburg.de>),
 */
public class VariableDouble implements EncapsulatedDouble {
    private double ohm;
    
    public VariableDouble(double ohm){
        this.ohm = ohm;
    }
    
    public double getOhm() {
        return ohm;
    }

    public void setOhm(double ohm) {
        this.ohm = ohm;
    }
    
    public String toString() {
        return String.format("[<%s>:%.2f]", VariableDouble.class.getSimpleName(),ohm);
    }
}
